package com.gmail.adrianmilas.xchange;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by adria on 13-Feb-18.
 */

public class XMLReader {

    private static Document readFile(File file){
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Document doc = null;
        try {
            doc = dBuilder.parse(file);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return doc;
    }

    public static HashMap<String, Double> getRates(File file) throws IOException{
        HashMap<String, Double> rates = new HashMap<>();
        Document doc = readFile(file);
        NodeList nList = doc.getElementsByTagName("Rate");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                NamedNodeMap attributes = eElement.getAttributes();
                Node ns = attributes.getNamedItem("currency");
                String symbol = ns.getNodeValue();
                String strValue = eElement.getFirstChild().getNodeValue();
                Node ns2 = attributes.getNamedItem("multiplier");
                String multiplier = null;
                if (ns2 != null)
                    multiplier = ns2.getNodeValue();
                Double value = Double.parseDouble(strValue);
                rates.put(symbol, value);
            }
        }
        return rates;
    }

    public static String getDate(File file){
        Document doc = readFile(file);
        NodeList nList = doc.getElementsByTagName("PublishingDate");
        Element nNode = (Element) nList.item(0);
        String strValue = nNode.getFirstChild().getNodeValue();
        return strValue;
    }

    public static boolean checkFile(File file){
        boolean check = true;
        if (!file.exists() || file.length() == 0) {
            return false;
        }
        Document doc = readFile(file);
        if (doc == null)
            return false;
//        URL schemaFile = null;
//        try {
//            schemaFile = new URL("http://www.bnr.ro/xsd/nbrfxrates.xsd");
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }

        return check;
    }
}
