package com.gmail.adrianmilas.xchange;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by adria on 13-Feb-18.
 */

public class TimeControl {
    public static Calendar c = initialize();
    private static SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");

    private static Calendar initialize() {
        Calendar c = Calendar.getInstance();
        System.out.println(c.get(Calendar.HOUR_OF_DAY));
        c.setTimeZone(TimeZone.getTimeZone("Europe/Bucharest"));
        System.out.println(c.get(Calendar.HOUR_OF_DAY));
        c.setFirstDayOfWeek(Calendar.MONDAY);
        return c;
    }

    public static boolean isBankDay(){
        int weekday = c.get(Calendar.DAY_OF_WEEK);
        if (weekday == Calendar.SATURDAY || weekday == Calendar.SUNDAY){
            return false;
        }
        else return true;
    }

    public static String getLatestRefreshTime(){
        // new rates are given everyday at 13:00
        Date date;
        int hour = c.get(Calendar.HOUR_OF_DAY);
        if (isBankDay() && hour >= 13){
            date = c.getTime();
        } else {
            date = getLastBankDay();
        }
        String dst = dt.format(date);
        return dst;
    }

    private static Date getLastBankDay() {
        Date date;
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        if (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY || dayOfWeek == Calendar.MONDAY){
            c.add(Calendar.DAY_OF_WEEK, Calendar.FRIDAY - dayOfWeek);
            date = c.getTime();
            System.out.println("last friday: " + date);
        } else {
            c.add(Calendar.DATE, -1);
            date = c.getTime();
        }
        return date;
    }

    public static void main(String[] args) {
        String date = getLatestRefreshTime();
        System.out.println(date);
    }
}
