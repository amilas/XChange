package com.gmail.adrianmilas.xchange;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

/**
 * Created by adria on 13-Feb-18.
 */

public class ExchangeRates {
    private static ExchangeRates instance = null;
    private static InputStream is;
    private static OutputStream os;
    private static String fileName;

    private HashMap<String, Double> rates;

    public static ExchangeRates getInstance(){
        File file = new File(fileName);
        if (!XMLReader.checkFile(file)) {
            importXML();
        }
       if (wasCalledToday()){
            if (instance == null)
                instance = new ExchangeRates();
            return instance;
        }
        else {
            importXML();
            instance = new ExchangeRates();
            return instance;
        }
    }

    ExchangeRates(){
        rates = getRatesFromXML();
    }

    public static void setContext(Context context) {
        fileName = context.getFilesDir().getPath().toString() +  "/exchange.xml";
    }

    private HashMap<String, Double> getRatesFromXML() {
        HashMap<String, Double> result = new HashMap<>();
        File file = new File(fileName);
        try {
            result = XMLReader.getRates(file);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }

    public double getTodayRate(String currencySymbol){
        Double value;
        if (currencySymbol.equals("RON"))
            value = 1.00;
        else
            value = rates.get(currencySymbol);
        return value;
    }

    public static String getDate(){
        String dateX = XMLReader.getDate(new File(fileName));
        return dateX;
    }

    public static void importXML(){
        URL url = null;
        try {
            url = new URL("http://www.bnr.ro/nbrfxrates.xml");

            // Get the input stream through URL Connection
            URLConnection con = url.openConnection();
            is = con.getInputStream();
            File file = new File(fileName);
            os = new FileOutputStream(file);

            copyStream(is, os);

            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String line = null;

            // read each line and write to System.out
            System.out.println("start reading");
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
            System.out.println("stop reading");
            is.close();
            os.flush();
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally{

        }
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException
    {
        byte[] buffer = new byte[1024]; // Adjust if you want
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1)
        {
            output.write(buffer, 0, bytesRead);
        }
    }

    private static boolean wasCalledToday(){
        String dateStr = XMLReader.getDate(new File(fileName));
        String shouldBe = TimeControl.getLatestRefreshTime();
        if (dateStr.equals(shouldBe))
            return true;
        return false;
    }

    public static void main(String[] args) {
        ExchangeRates x = getInstance();
        Double r = x.getTodayRate("USD");
        System.out.println(r);
    }
}
