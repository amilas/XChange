package com.gmail.adrianmilas.xchange;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class MainActivity extends Activity {

    Button mButton;
    EditText mEdit;
    EditText mFinal;
    Spinner s1, s2;
    TextView date, c1, c2;
    String symbol1 = null, symbol2 = null;
    Double rate1 = 1.0;
    Double rate2 = 1.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Context context = getApplicationContext();
        ExchangeRates.setContext(context);

        mEdit   = (EditText)findViewById(R.id.sumaInitiala);
        mFinal = (EditText)findViewById(R.id.valEuro);
        mButton = (Button)findViewById(R.id.goButton);
        s1 = (Spinner)findViewById(R.id.valuta1);
        s2 = (Spinner)findViewById(R.id.valuta2);
        s1.setSelection(0);
        s2.setSelection(1);

        date = (TextView)findViewById(R.id.data);
        String d = ExchangeRates.getDate();
        date.setText(d);

        c1 = (TextView)findViewById(R.id.curs1);
        c2 = (TextView)findViewById(R.id.curs2);

        mEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEdit.setText("");
                mFinal.setText("");
            }
        });

        s1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                symbol1 = s1.getItemAtPosition(s1.getSelectedItemPosition()).toString();
                rate1 = ExchangeRates.getInstance().getTodayRate(symbol1);
                mEdit.setHint(symbol1);
                mFinal.setText("");
                if (!symbol1.equals("RON")) {
                    String sv = symbol1 + " = " + rate1 + "RON";
                    c1.setText(sv);
                }  else {
                    c2.setText("");
                }
                if(symbol1.equals(symbol2)){
                    c1.setText("");
                    c2.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        s2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                symbol2 = s2.getItemAtPosition(s2.getSelectedItemPosition()).toString();
                rate2 = ExchangeRates.getInstance().getTodayRate(symbol2);
                mFinal.setHint(symbol2);
                mFinal.setText("");
                if (!symbol2.equals("RON")) {
                    String sv = symbol2 + " = " + rate2 + "RON";
                    c2.setText(sv);
                }  else {
                    c2.setText("");
                }
                if(symbol1.equals(symbol2)){
                    c1.setText("");
                    c2.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double value = Double.parseDouble(mEdit.getText().toString());
                double result = getResult(value, rate1, rate2);
                result = round(result, 2);
                DecimalFormat df = new DecimalFormat("#.00");
                mFinal.setText(df.format(result));
            }
        });
    }

    public static double getResult(Double sum, Double rate1, Double rate2){
        double result;
        result = sum * rate1/rate2;
        return result;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_DOWN);
        return bd.doubleValue();
    }
}
